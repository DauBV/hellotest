//
//  ViewController.swift
//  Helloword_Webview
//
//  Created by Dậu BV on 22/12/2021.
//

import UIKit
import WebKit

class ViewController: UIViewController {
    var url: URL!
    var wKWebView: WKWebView!
    
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var indicatorLoad: UIActivityIndicatorView!
    var btnback: UIBarButtonItem!
    var btnForward: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(tappedRefresh))
        btnback = UIBarButtonItem(title: "<<<", style: .plain, target: self, action: #selector(tappedBack))
        
        btnForward = UIBarButtonItem(title: ">>>", style: .plain, target: self, action: #selector(tappedForWard))
        
        navigationItem.rightBarButtonItems = [btnForward, btnback]
        
        url = URL(string: "https://www.24h.com.vn/")
        wKWebView = WKWebView(frame: view.frame)
        containerView.addSubview(wKWebView)
        wKWebView.navigationDelegate = self
        wKWebView.uiDelegate = self
        let request = URLRequest(url: url)
        wKWebView.load(request)
    }
    
    @objc func tappedRefresh() {
        wKWebView.reload()
    }
    
    @objc func tappedBack() {
        wKWebView.goBack()
    }
    
    @objc func tappedForWard() {
        wKWebView.goForward()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
//        wKWebView.frame = createWKWebViewFrame(size: size)
    }
}

extension ViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        // show indicator
        indicatorLoad.isHidden = false
        indicatorLoad.startAnimating()
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        // dismiss indicator
      
        // if url is not valid {
        //    decisionHandler(.cancel)
        // }
        decisionHandler(.allow)
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        // dismiss indicator
        indicatorLoad.isHidden = true
        indicatorLoad.stopAnimating()
        btnback.isEnabled = webView.canGoBack
        btnForward.isEnabled = webView.canGoForward
        navigationItem.title = webView.title
    }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
      // show error dialog
    }
}

extension ViewController: WKUIDelegate {
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if navigationAction.targetFrame == nil {
            webView.load(navigationAction.request)
        }
        return nil
    }
}

